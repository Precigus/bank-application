﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bank_Application.Project1.Models;

namespace Bank_Application.Project1.Services
{
    public class AuthService
    {
        private readonly IEnumerable<Customer> _customers;

        public AuthService(IEnumerable<Customer> customers)
        {
            _customers = customers;
        }

        public Customer Authenticate()
        {
            Customer customer;
            bool isSuccess;
            do
            {
                var credentials = GetCredentials();
                isSuccess = Login(_customers, credentials, out customer);
            } while (!isSuccess);

            return customer;
        }

        private Credentials GetCredentials()
        {
            var credentials = new Credentials();

            Console.Write("Enter your username: ");
            var username = Console.ReadLine();
            credentials.Username = username;

            Console.Write("Enter your password: ");
            var password = Console.ReadLine();
            credentials.Password = password;

            return credentials;
        }

        public bool Login(IEnumerable<Customer> customers, Credentials credentials, out Customer loggedInCustomer)
        {
            loggedInCustomer = customers.First(c => c.Name == credentials.Username);
            
            if (loggedInCustomer == null)
            {
                return false;
            }

            return loggedInCustomer.CheckPassword(credentials.Password);
        }

    }
}