﻿using System.Linq;
using Bank_Application.Project1.Models;
using Bank_Application.Project1.Repository.Context;

namespace Bank_Application.Project1.Repository.File.Repository
{
    public class CustomerRepository : Repository<Customer>
    {
        public CustomerRepository(BankContext context)
        {
            Context = context.Customers.ToList();
        }

        public string GetCustomerNameByBankAccountId(uint id)
        {
            var customerName = Context
                .First(c => c.Accounts
                    .Any(a => a.Id == id)
                ).Name;

            return customerName;
        }
    }
}