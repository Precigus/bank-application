﻿using System.Collections.Generic;
using Bank_Application.Project1.Repository.Interfaces;

namespace Bank_Application.Project1.Repository.File.Repository
{
    public class Repository<TModel> where TModel: class, IEntity
    {
        protected List<TModel> Context;
        protected uint Id { get; set; }

        protected Repository(){}

        public void Create(TModel model)
        {
            Context.Add(model);
        }

        public void Update(TModel model)
        {
            
        }

        public void Delete(uint key)
        {
            Context.RemoveAll(i => i.Id == key);
        }

        public IEnumerable<TModel> Get()
        {
            return Context;
        }

        public TModel Get(uint key)
        {
            var model = Context.Find(m => m.Id == key);

            return model;
        }
    }
}