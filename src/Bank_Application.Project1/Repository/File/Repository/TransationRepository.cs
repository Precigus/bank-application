﻿using System.Linq;
using Bank_Application.Project1.Models;
using Bank_Application.Project1.Repository.Context;

namespace Bank_Application.Project1.Repository.File.Repository
{
    public class TransationRepository : Repository<Transaction>
    {
        public TransationRepository(BankContext context)
        {
            Context = context.Transactions.ToList();
        }
    }
}