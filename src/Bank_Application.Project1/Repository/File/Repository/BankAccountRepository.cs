﻿using System.Linq;
using Bank_Application.Project1.Models;
using Bank_Application.Project1.Repository.Context;

namespace Bank_Application.Project1.Repository.File.Repository
{
    public class BankAccountRepository : Repository<BankAccount>
    {
        public BankAccountRepository(BankContext context)
        {
            Context = context.BankAccounts.ToList();
        }
    }
}