﻿namespace Bank_Application.Project1.Repository.Interfaces
{
    public interface IEntity
    {
        uint Id { get; set; }
    }
}