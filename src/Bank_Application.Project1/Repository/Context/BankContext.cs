﻿using System.Collections.Generic;
using System.Linq;
using Bank_Application.Project1.Enums;
using Bank_Application.Project1.Models;

namespace Bank_Application.Project1.Repository.Context
{
    public class BankContext: IBankContext
    {
        public virtual IList<Customer> Customers { get; set; }
        public virtual IList<BankAccount> BankAccounts { get; set; }
        public virtual IList<Transaction> Transactions { get; set; }

        public BankContext()
        {
            BankAccounts  = new List<BankAccount>
            {
                new BankAccount(11, AccountType.Chequing, 2580455.86),
                new BankAccount(12, AccountType.Savings, 458563.15),
                new BankAccount(13, AccountType.Savings, 885423.25),
                new BankAccount(14, AccountType.Chequing, 78228.33),
            };

            Transactions = new List<Transaction>
            {
                new Transaction(1, BankAccounts[0], BankAccounts[1], 1012.15),
                new Transaction(2, BankAccounts[1], BankAccounts[2], 158.55),
                new Transaction(3, BankAccounts[2], BankAccounts[1], 8858.56),
                new Transaction(4, BankAccounts[1], BankAccounts[3], 2236.63),
            };

            BankAccounts[0].Transactions.AddRange(GetTransactions(BankAccounts[0]));
            BankAccounts[1].Transactions.AddRange(GetTransactions(BankAccounts[1]));
            BankAccounts[2].Transactions.AddRange(GetTransactions(BankAccounts[2]));
            BankAccounts[3].Transactions.AddRange(GetTransactions(BankAccounts[3]));

            Customers = new List<Customer>
            {
                new Customer(1, "James","Cook", new [] {BankAccounts[0], BankAccounts[1]}),
                new Customer(1, "Randal","Password123", new [] {BankAccounts[2]}),
                new Customer(1, "Oliver","Cashews15", new [] {BankAccounts[3]}),
            };
        }

        public IEnumerable<Transaction> GetTransactions(BankAccount account)
        {
            return Transactions.Where(t => t.IsTransactionForAccount(account));
        }
    }
}