﻿using System.Collections;
using System.Collections.Generic;
using Bank_Application.Project1.Models;

namespace Bank_Application.Project1.Repository.Context
{
    public interface IBankContext
    {
        IList<Customer> Customers { get; set; }
        IList<BankAccount> BankAccounts { get; set; }
        IList<Transaction> Transactions { get; set; }
    }
}