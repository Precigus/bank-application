﻿using System.Collections.Generic;
using Bank_Application.Project1.Enums;
using Bank_Application.Project1.Repository.Interfaces;

namespace Bank_Application.Project1.Models
{
    public class BankAccount: IEntity
    {
        public uint Id { get; set; }
        public AccountType AccountType { get; }
        public double Balance { get; }
        public List<Transaction> Transactions { get; }


        public BankAccount(uint id, AccountType accountType, double balance)
        {
            Id = id;
            AccountType = accountType;
            Balance = balance;
            Transactions = new List<Transaction>();
        }
    }
}