﻿using System.Collections.Generic;
using Bank_Application.Project1.Repository.Interfaces;

namespace Bank_Application.Project1.Models
{
    public class Customer:IEntity
    {
        public uint Id { get; set; }
        public string Name { get; }
        private string Password { get; }
        public IEnumerable<BankAccount> Accounts { get; }

        public Customer(uint id, string name, string password, IEnumerable<BankAccount> accounts)
        {
            Id = id;
            Name = name;
            Password = password;
            Accounts = accounts;
        }

        public bool CheckPassword(string password)
        {
            return Password == password;
        }
    }
}