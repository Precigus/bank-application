﻿using Bank_Application.Project1.Repository.Interfaces;

namespace Bank_Application.Project1.Models
{
    public class Transaction: IEntity
    {
        public uint Id { get; set; }
        public BankAccount From { get; }
        public BankAccount To { get; }
        public double Amount { get; }

        public Transaction(uint id, BankAccount from, BankAccount to, double amount)
        {
            Id = id;
            From = from;
            To = to;
            Amount = amount;
        }

        public bool IsTransactionForAccount(BankAccount account)
        {
            return From.Id == account.Id || To.Id == account.Id;
        }

        public bool IsOutgoing(BankAccount bankAccount)
        {
            return From.Id == bankAccount.Id;
        }
    }
}