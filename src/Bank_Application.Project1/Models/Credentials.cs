﻿namespace Bank_Application.Project1.Models
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}