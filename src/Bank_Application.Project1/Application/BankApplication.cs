﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Bank_Application.Project1.Models;
using Bank_Application.Project1.Services;
using Bank_Application.Project1.Repository.File.Repository;

namespace Bank_Application.Project1.Application
{
    public class BankApplication
    {
        private readonly CustomerRepository _customerRepository;

        public BankApplication(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public void PrintInfo()
        {
            var customers = _customerRepository.Get();

            var currentUser = PromptAuthorization(customers);

            Thread.Sleep(2000);

            ShowAccountInformation(currentUser);

            Console.ReadLine();
        }

        private Customer PromptAuthorization(IEnumerable<Customer> customers)
        {
            var authentication = new AuthService(customers);
            Customer currentUser = null;
            try
            {
                currentUser = authentication.Authenticate();
            }
            catch (Exception e)
            {
                Console.WriteLine("Incorrect username and/or password! Check your credentials and try again");
                Console.ReadLine();
                Console.Clear();
                PrintInfo();
            }

            Console.WriteLine("Login successful");
            
            return currentUser;
        }

        private void ShowAccountInformation(Customer customer)
        {
            Console.Clear();
            Console.WriteLine($"Account Owner: {customer.Name}");

            foreach (var bankAccount in customer.Accounts)
            {
                var transactions = bankAccount.Transactions;
                Console.WriteLine($"\nAccount Type: {bankAccount.AccountType}, Account Balance: {bankAccount.Balance}");
                Console.WriteLine("*=====================================================*");

                if (transactions.Any())
                {
                    foreach (var transaction in transactions)
                    {
                        var sender = GetTransactionParticipant(transaction.From.Id);
                        var receiver = GetTransactionParticipant(transaction.To.Id);
                        var modifier = "";

                        if (transaction.IsOutgoing(bankAccount))
                        {
                            modifier = "-";
                        }

                        Console.WriteLine($"From: {sender}   |   To: {receiver}   |   Amount: {modifier}{transaction.Amount}");
                    }
                }
                else
                {
                    Console.WriteLine("There are no transactions for this account!");
                }
                                    
                Console.WriteLine("*-----------------------------------------------------*");
            }
        }

        private string GetTransactionParticipant(uint id)
        {
            return _customerRepository.GetCustomerNameByBankAccountId(id);
        }
    }
}