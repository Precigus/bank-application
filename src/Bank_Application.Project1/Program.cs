﻿using Bank_Application.Project1.Application;
using Bank_Application.Project1.Repository.Context;
using Bank_Application.Project1.Repository.File.Repository;

namespace Bank_Application.Project1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var context = new BankContext();

            var app = new BankApplication(new CustomerRepository(context));

            app.PrintInfo();
        }
    }
}